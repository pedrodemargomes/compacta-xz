CC = gcc
LDFLAGS = -lpthread
DEBUG = -DDEBUG

all: compacta

compacta: compacta.o
	$(CC) $< $(LDFLAGS) -o $@

debug: compacta.c
	$(CC) $< $(LDFLAGS) $(DEBUG) -o $@

clean: compacta.o
	rm -f $<

purge: clean
	rm -f compacta debug
