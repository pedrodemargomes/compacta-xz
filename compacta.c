#define _GNU_SOURCE
#include <dirent.h>     /* Defines DT_* constants */
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <stdint.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <stdbool.h>

#define MAX_QUEUE 128
#define BUF_SIZE 1024

struct Job {
    void (*fun)(void *arg);
    char arg[PATH_MAX];
};

struct Pool {
    int head;
    int tail;
    struct Job *queue[MAX_QUEUE];
    int size;
    pthread_mutex_t lock;
    pthread_cond_t workCond;
    pthread_t *poolThreads;
    int numThreads;
    bool stop;
};

int isFull(struct Pool *p) {
	return (p->size == MAX_QUEUE);
}

int isEmpty(struct Pool *p) {
	return (p->size == 0);
}

int enqueue(struct Pool *p, struct Job *elem) {
    if(isFull(p))
        return -1;
    p->tail = (p->tail + 1) % MAX_QUEUE;
    p->queue[p->tail] = elem; 
    p->size++;
    return 0;
}

struct Job *dequeue(struct Pool *p) {
	if(isEmpty(p))
		return NULL;
    struct Job *elem = p->queue[p->head]; 
    p->head = (p->head + 1) % MAX_QUEUE;
    p->size--;
	return elem;
}

int runPool(void *p_) {
    #ifdef DEBUG
    printf("START THREAD POOL\n");
    #endif

    struct Pool *p = (struct Pool *) p_;

    for(;;) {
        pthread_mutex_lock(&(p->lock));
        if(isEmpty(p))
            pthread_cond_wait(&(p->workCond), &(p->lock));

        struct Job *j = dequeue(p);
        pthread_mutex_unlock(&(p->lock));
        if(j != NULL) {
            #ifdef DEBUG
            printf("=> START JOB | ARG = %s\n", j->arg);
            #endif
            j->fun(j->arg);
            #ifdef DEBUG
            printf("=> END JOB | ARG = %s\n", j->arg);
            #endif
        }

        pthread_mutex_lock(&(p->lock));
        if(p->stop && p->size == 0) {
            pthread_cond_broadcast(&(p->workCond));
            break;
        }
        pthread_mutex_unlock(&(p->lock));

    }
    pthread_mutex_unlock(&(p->lock));

    #ifdef DEBUG
    printf("END THREAD POOL\n");
    #endif
}

int initThreadPool(struct Pool *p, int numThreads) {
    pthread_mutex_init(&(p->lock), NULL);
    pthread_cond_init(&(p->workCond), NULL);
    p->head = 0;
    p->tail = -1;
    p->size = 0;
    p->stop = false;
    p->numThreads = numThreads;
    pthread_t *t = malloc(numThreads*sizeof(pthread_t));
    for(int i = 0;i< numThreads; i++)
        pthread_create(&t[i], NULL, runPool, (void *)p);
    p->poolThreads = t;
    return 0;
}

int addWork(struct Pool *p, struct Job *j) {
    pthread_mutex_lock(&(p->lock));
    enqueue(p, j);
    pthread_cond_signal(&(p->workCond));
    pthread_mutex_unlock(&(p->lock));
}

int waitPool(struct Pool *p) {
    pthread_mutex_lock(&(p->lock));
    p->stop = true;
    pthread_cond_broadcast(&(p->workCond));
    pthread_mutex_unlock(&(p->lock));
    for(int i=0;i<p->numThreads;i++)
        pthread_join(p->poolThreads[i], NULL);
    return 0;
}

int destroyThreadPool(struct Pool *p) {

}

void compacta(void *path_) {
    char *path = (char *)path_;
    // *** Run xz ***
    // printf("XZ %s\n", path);
    for(int i=0; i < 0xFFFFFFFF; i++) {}
    // ...
    // **************
}

struct linux_dirent64 {
    ino64_t        d_ino;    /* 64-bit inode number */
    off64_t        d_off;    /* 64-bit offset to next structure */
    unsigned short d_reclen; /* Size of this dirent */
    unsigned char  d_type;   /* File type */
    char           d_name[]; /* Filename (null-terminated) */
};

int main() {
    
    struct Pool p;
    initThreadPool(&p, 4);

    int fd = openat(AT_FDCWD, ".",  O_RDONLY | O_DIRECTORY);
    struct linux_dirent64 *dirp;
    uint8_t buf[BUF_SIZE]; 
    int nb = syscall(SYS_getdents64, fd, buf, BUF_SIZE);
    for(int i = 0; i < nb;) {
        dirp = (struct linux_dirent64 *) (buf+i);
        // printf("%s %s\n", (dirp->d_type == DT_DIR ? "diretorio" : "-"), dirp->d_name );

        struct Job *j = malloc(sizeof(struct Job));
        j->fun = &compacta;
        strcpy(j->arg, dirp->d_name);
        addWork(&p, j);

        i += dirp->d_reclen;
    }

    #ifdef DEBUG
    printf("WAIT FOR THREADS\n");
    #endif
    waitPool(&p);

    #ifdef DEBUG
    printf("END MAIN\n");
    #endif
    return 0;
}
